<?php

namespace Kowal\Amp\Model\Plugin\Framework;

use Magento\Framework\UrlInterface;

/**
 * Plugin for processing builtin cache
 */
class Url
{
    /**
     * @var \Kowal\Amp\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @param \Kowal\Amp\Helper\Data $dataHelper
     */
    public function __construct(
        \Kowal\Amp\Helper\Data $dataHelper
    ) {
        $this->_dataHelper = $dataHelper;
    }

    /**
     * Add amp parameter for each url
     * @param  \Magento\Framework\UrlInterface $subject
     * @param  string
     * @return string
     */
    public function afterGetUrl(UrlInterface $subject, $url)
    {
        if ($this->_dataHelper->isAmpCall()){
            return $this->_dataHelper->getAmpUrl($url);
        }

        return $url;
    }

}
