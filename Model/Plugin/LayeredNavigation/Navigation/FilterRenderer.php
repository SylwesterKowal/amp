<?php

namespace Kowal\Amp\Model\Plugin\LayeredNavigation\Navigation;

use Magento\Catalog\Model\Layer\Filter\FilterInterface;


class FilterRenderer
{
    /**
     * @var \Kowal\Amp\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @param \Kowal\Amp\Helper\Data $dataHelper
     */
    public function __construct(
        \Kowal\Amp\Helper\Data $dataHelper
    ) {
        $this->_dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\LayeredNavigation\Block\Navigation\FilterRenderer $subject
     * @param callable $proceed
     * @param FilterInterface $filter
     * @return string
     */
    public function aroundRender(
        \Magento\LayeredNavigation\Block\Navigation\FilterRenderer $subject,
        callable $proceed,
        FilterInterface $filter
    )
    {
        if ($this->_dataHelper->isAmpCall()) {
            $filters = $filter->getItems();
            $subject->assign('filterItems', $filters);
            $subject->setTemplate('Magento_LayeredNavigation::layer/filter.phtml');
            $html = $subject->toHtml();
            $subject->assign('filterItems', []);
            return $html;
        }
        return $proceed($filter);
    }
}
