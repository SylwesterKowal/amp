<?php

namespace Kowal\Amp\Model\Plugin\Cms\Helper;

use Magento\Framework\App\Action\Action;

class PagePlugin
{
    /**
     * @var \Kowal\Amp\Helper\Data
     */
    protected $dataHelper;

    /**
     * @param \Kowal\Amp\Helper\Data $dataHelper
     * @return  void
     */
    public function __construct(
        \Kowal\Amp\Helper\Data $dataHelper
    ) {
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param  controller
     * @param  \Magento\Framework\App\Action\Action $action
     * @param  string                               $pageId
     * @return array
     */
    public function beforePrepareResultPage($subject, $action, $pageId = null)
    {
        /**
         * Set PageId amp-homepage
         * (Only for amp request)
         */
        if ($this->dataHelper->isAmpCall() && 'cms_index_index' == $this->dataHelper->getFullActionName()) {
            $pageId = \Kowal\Amp\Helper\Data::AMP_HOME_PAGE_KEYWORD;
        }

        return [$action, $pageId];
    }

}
