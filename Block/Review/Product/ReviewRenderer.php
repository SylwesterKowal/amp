<?php

namespace Kowal\Amp\Block\Review\Product;

class ReviewRenderer extends \Magento\Review\Block\Product\ReviewRenderer
{

    protected $_availableTemplates = [
        self::SHORT_VIEW => 'Kowal_Amp::review/helper/summary_short.phtml',
        self::FULL_VIEW => 'Kowal_Amp::review/helper/summary.phtml',
    ];

}
