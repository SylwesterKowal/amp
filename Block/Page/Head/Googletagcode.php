<?php
namespace Kowal\Amp\Block\Page\Head;

class Googletagcode extends \Magento\Framework\View\Element\Text
{
    /**
     * @var \Kowal\Amp\Helper\Data
     */
    protected $_dataHelper;


    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Kowal\Amp\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Kowal\Amp\Helper\Data $dataHelper
    ) {
        $this->_dataHelper = $dataHelper;
        parent::__construct($context);
    }

    /**
     * Override parent method
     * @var void
     * @return string
     */
    public function getText()
    {
        return trim($this->_dataHelper->getGoogleTagCode());
    }
}
