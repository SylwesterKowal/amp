<?php

namespace Kowal\Amp\Block\Page\Head\Kowal;

class Cms extends AbstractKowal
{
	/**
	 * Retrieve additional data
	 * @return array
	 */
    public function getKowalParams()
    {
        $params = parent::getKowalParams();
        return array_merge($params, [
            'type' => 'website',
            'url' => $this->_helper->getCanonicalUrl(
                $this->getUrl('*/*/*', ['_current' => true, '_use_rewrite' => true])
            ),
        ]);
    }
}
